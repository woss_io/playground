use failure::Error;

fn main() -> Result<(), Error>{
    let mut args = std::env::args().skip(1);
    
    match args.next().as_ref().map(|f| &f[..]) {
        Some("help") => help(),
        Some(arg) if gpg_sign_arg(arg)  => {
            println!("got the non default hand, {:?}", arg);
            verify_commit()},
        _                               => {
            if args.any(|arg| gpg_sign_arg(&arg)) {
                println!("got default hand will verify_commit");

                verify_commit()
            } else {
                println!("got default hand will delegate");

                delegate()
            }
        }
    }
}

fn gpg_sign_arg(arg: &str) -> bool {
    arg == "--sign" || (arg.starts_with("-") && !arg.starts_with("--") && arg.contains("s"))
}


fn help()-> Result<(), Error> {
    println!("This is the help ");
    Ok(())
}
fn verify_commit()-> Result<(), Error> {
    use std::io::Read;
    let mut commit = String::new();
    let mut stdin = std::io::stdin();
    stdin.read_to_string(&mut commit)?;

    println!("\n singin the commit with data {:?}", commit);
    Ok(())
}

fn delegate() -> ! {
    use std::process;

    let mut cmd = process::Command::new("gpg2");
    cmd.args(std::env::args().skip(1));
    let status = cmd.status().unwrap().code().unwrap();
    process::exit(status)
}
