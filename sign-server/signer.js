
import nodeFetch from 'https://cdn.skypack.dev/node-fetch';

async function main() {
	process.stdin.on("data", async (data) => {
		const commitMsg = data;
		const response = await fetch("http://localhost:31111/sign", {
			method: "POST",
			body: commitMsg.toString(),
		});
		const { signature } = await response.json();

		// logger.info(`[GNUPG:] KEY_CONSIDERED ${privateKey.getKeyID().toHex()} 0`)
		// // process.stdout.write(`[GNUPG:] KEY_CONSIDERED ${privateKey.getKeyID().toHex()} 0`)

		// logger.info("[GNUPG:] BEGIN_SIGNING H10")
		// // process.stdout.write("[GNUPG:] BEGIN_SIGNING H10")
		// // somehow this must be in the sterr so git can recognize it
		process.stderr.write(`\n[GNUPG:] SIG_CREATED `);
		// // this must be written to the stdout so git can pick up the signature
		process.stdout.write(signature);
	});
	return;
}

main();
