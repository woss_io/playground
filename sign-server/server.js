import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io';
import * as openpgp from 'openpgp';


async function main() {
	const app = express()
	const httpServer = createServer(app);

	const io = new Server(httpServer);

	const port = 31111

	app.get('/', (req, res) => {
		res.send('Hello World!')
	})
	app.post('/sign', async (req, res) => {
		console.log(req.body)

		const message = await openpgp.createMessage({ binary: commitMsg });
		const detachedSignature
			= await openpgp.sign({
				message, // Message object
				signingKeys: privateKey,
				detached: true
			});
	})

	io.on('connection', (socket) => {
		console.log('a user connected');
	});

	httpServer.listen(port, () => {
		console.log(`Example app listening on port ${port}`)
	})
}


async function readKeys() {
	return {
		publicKey: (await readFile(`${absPath}/keys/public.key`)).toString(),
		privateKey: (await readFile(`${absPath}/keys/private.key`)).toString(),
		passPhrase: (await readFile(`${absPath}/keys/passphrase`)).toString()
	}
}

main().catch(console.error)